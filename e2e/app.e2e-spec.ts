import { NgWithPrimeNGPage } from './app.po';

describe('ng-with-prime-ng App', () => {
  let page: NgWithPrimeNGPage;

  beforeEach(() => {
    page = new NgWithPrimeNGPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
